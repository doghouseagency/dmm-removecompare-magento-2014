<?php

/**
 * Make getAddUrl return false as suggested in 
 * http://www.magentocommerce.com/wiki/4_-_themes_and_template_customization/catalog/how_to_remove_add_to_compare
 */

class Doghouse_RemoveCompare_Helper_Product_Compare extends Mage_Catalog_Helper_Product_Compare {

    public function getAddUrl($product) {
        return false;
    }

}