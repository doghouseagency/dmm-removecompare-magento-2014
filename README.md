# Remove Compare

Removes the compare product feature in Magento.

Read more:

http://www.magentocommerce.com/wiki/4_-_themes_and_template_customization/catalog/how_to_remove_add_to_compare